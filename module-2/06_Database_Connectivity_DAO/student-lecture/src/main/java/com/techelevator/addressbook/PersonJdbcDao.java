package com.techelevator.addressbook;

import java.util.ArrayList;
import java.util.List;

import javax.activation.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class PersonJdbcDao {
	
	private JdbcTemplate jdbcTemplate;
	
	public PersonJdbcDao(DataSource datasource) {
		jdbcTemplate = new JdbcTemplate(datasource);
		
	}
	
	
	
	@Override
	public List<Person> list() {
		
		List<Person> persons = new ArrayList<Person>();
		
		String selectSql = "SELECT * FROM person_id, first_name, last_name, date_added FROM person";
		
		SqlRowSet rows = jdbcTemplate.queryForRowSet(selectSql);
		
		while (rows.next()) {
			Person person = mapRowToPerson(rows);
			persons.add(person);
		}
	return persons;
	}
	
	@Override
	public Person getPersonById(int id) {
		
		return null;
	}

	
	private Person mapRowToPerson (SqlRowSet row) {
		
		Person person = new Person();
		
		person.setPersonId(row.getInt("person_id"));
		person.setFirstName(row.getString("first_name"));
		person.setLastName(row.getString("last_name"));
		
		if( row.getDate("date_added") != null) {
		person.setDateAdded(row.getDate("date_added").toLocalDate());
		}
		return person;
	}
}
