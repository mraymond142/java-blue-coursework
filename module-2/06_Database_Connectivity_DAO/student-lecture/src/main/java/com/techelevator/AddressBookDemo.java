package com.techelevator;

import javax.activation.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import com.techelevator.addressbook.PersonDao;
import com.techelevator.addressbook.PersonJdbcDao;

public class AddressBookDemo {

	public static void main(String[] args) {
		
		BasicDataSource datasource = new BasicDataSource();
		datasource.setUrl("jdbc:postgresql://localhost:5432/world");
		datasource.setUsername("postgres");
		datasource.setPassword("postgres1");
		
		PersonDao personDao = new PersonJdbcDao(datasource);

	}

}
