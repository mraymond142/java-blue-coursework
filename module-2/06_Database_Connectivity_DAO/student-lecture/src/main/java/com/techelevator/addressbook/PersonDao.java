package com.techelevator.addressbook;

import java.util.List;

public interface PersonDao {
	
	List<Person> list();
	PersonDao getPersonById(int id);
	
	

}
