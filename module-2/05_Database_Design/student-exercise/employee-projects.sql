-- employee-projects

DROP TABLE employee;
DROP TABLE department;
DROP TABLE employee_department;
DROP TABLE project;
DROP TABLE project_employee;

CREATE TABLE employee (

        employee_number serial primary key,
        job_title varchar(10) NOT NULL,
        first_name varchar(30) NOT NULL,
        last_name varchar(30) NOT NULL,
        gender varchar(10),
        date_of_birth date NOT NULL,
        date_of_hire date NOT NULL,
        department_number int, 
        CONSTRAINT check_title check (job_title = 'Developer' OR job_title = 'QA' OR job_title = 'Team lead')
);



CREATE TABLE department (

        
        department_number serial primary key,
        employee_number serial,
        department_name varchar(30) NOT NULL,
        num_employees int NOT NULL, CONSTRAINT num_employees CHECK (num_employees >= 2),
        FOREIGN KEY (employee_number) REFERENCES employee(employee_number)  
);


CREATE TABLE project (


        
        project_id serial primary key,
        employee_number serial,
        project_name varchar(50) NOT NULL,
        project_start_date date NOT NULL,
        num_employees int NOT NULL, CONSTRAINT num_employees CHECK (num_employees > 0),
        FOREIGN KEY (employee_number) REFERENCES employee(employee_number)  
  
);




INSERT INTO department (department_number, employee_number, department_name, num_employees)
VALUES (DEFAULT, DEFAULT, 'RYYV', 5);

