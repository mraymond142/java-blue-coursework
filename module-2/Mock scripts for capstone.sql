-- 1. All of the films that Nick Stallone has appeared in
-- (30 rows)

SELECT film.title
FROM actor
JOIN film_actor ON actor.actor_id = film_actor.actor_id
JOIN film ON film_actor.film_id = film.film_id
WHERE actor.first_name = 'NICK' AND actor.last_name = 'STALLONE';


SELECT name
FROM venue;

SELECT space.id, space.name, space.is_accessible, space.open_from, space.open_to, CAST(space.daily_rate AS decimal) AS rate, space.max_occupancy 
FROM space
JOIN venue ON venue.id = space.venue_id
WHERE venue.id = 2;

SELECT venue.name, (state.name) AS state, (city.name) AS city, venue.description, (category.name) AS category
FROM venue
JOIN city ON city.id = venue.city_id
JOIN state ON city.state_abbreviation = state.abbreviation
JOIN category_venue ON category_venue.venue_id = venue.id
JOIN category ON category.id = category_venue.category_id
WHERE venue.id = 7 
GROUP BY category.name, venue.name, state.name, city.name, venue.description
ORDER BY category.name, venue.name;

SELECT id, name, description
FROM venue
WHERE venue.id = 7
GROUP BY id, name, description
ORDER BY name;


SELECT space.name
FROM reservation
JOIN space ON reservation.space_id = space.id
WHERE ('2020/06/14', INTERVAL '50 day') OVERLAPS (start_date, end_date) = FALSE
GROUP BY space.name;

SELECT CAST(space.daily_rate AS DECIMAL) * 45 AS total_cost, space.daily_rate, space.name, space.max_occupancy
FROM space
JOIN reservation ON reservation.space_id = space.id
JOIN venue ON space.venue_id = venue.id
WHERE venue.id = 1 AND space.max_occupancy >= 23 AND ('2020/07/14', INTERVAL '5 day') OVERLAPS (reservation.start_date, reservation.end_date) = FALSE
GROUP BY space.name, space.daily_rate, space.max_occupancy
LIMIT 5;



--JOIN space ON space.id = reservation.space_id
--WHERE start_date = '2020/07/16';
--('2020/04/01', INTERVAL '2 day') OVERLAPS (open_from :: DATE ,open_to :: DATE)
SELECT CAST(SPLIT_PART('2020/12/13', '/' , 2)AS int) AS month_scheduled, open_to, open_from
FROM space;
JOIN space ON space.id = reservation.space_id
WHERE open_from = start_date;


SELECT CAST(SPLIT_PART('2020/12/13', '/' , 2)AS int) AS month_scheduled, open_to, open_from, ('2020/06/14', INTERVAL '50 day') OVERLAPS (start_date, end_date) 
FROM SPACE
JOIN reservation ON space.id = reservation.space_id;
WHERE ;
START TRANSACTION;
ROLLBACK;
Commit;

INSERT INTO reservation VALUES(Default, 12, 12, '2020/03/12', '2020/04/12', 'so');

SELECT (space.id) AS number, CAST(space.daily_rate  * 0 AS decimal) AS total_cost, CAST (space.daily_rate AS decimal) AS daily, (space.name) AS name, (space.max_occupancy) AS max_occupancy 
FROM space JOIN reservation ON reservation.space_id = space.id JOIN venue ON space.venue_id = venue.id 
WHERE venue.id = 4 AND space.max_occupancy >= 3 AND ('2020-07-23', '2020-07-29') OVERLAPS (reservation.start_date, reservation.end_date) = FALSE 
GROUP BY space.name, space.daily_rate, space.max_occupancy 
LIMIT 5;

INSERT INTO reservation VALUES (DEFAULT, 56, 456, '2020/06/12', '2020/07/12', 'i;mhere') RETURNING reservation_id;


INSERT INTO film_category VALUES(1001, 17);

SELECT setval('reservation_reservation_id_seq', (SELECT MAX(reservation_id) FROM reservation));

SELECT NEXTVAL('reservation_id')
FROM reservation;