-- Write queries to return the following:
-- The following changes are applied to the "dvdstore" database.**
START TRANSACTION;
COMMIT;
ROLLBACK;
-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.

SELECT * FROM actor;

INSERT INTO actor (first_name, last_name)
VALUES ('Hampton', 'Avenue'), ('Lisa', 'Byway');


-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in
-- ancient Greece", to the film table. The movie was released in 2008 in English.
-- Since its an epic, the run length is 3hrs and 18mins. There are no special
-- features, the film speaks for itself, and doesn't need any gimmicks.

SELECT * FROM film WHERE title = 'Euclidean PI';

INSERT INTO film (title, description, release_year, language_id, length)
VALUES ('Euclidean PI', 'The epic story of Euclid as a pizza delivery boy in
ancient Greece', 2008, 1, 198);

-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.

SELECT * 
FROM film_actor 
WHERE actor_id = 204;

INSERT INTO film_actor (actor_id, film_id)
VALUES (204, 1001), (205, 1001);


-- 4. Add Mathmagical to the category table.

SELECT * FROM category;
INSERT INTO category VALUES(17, 'Mathmagical');

-- 5. Assign the Mathmagical category to the following films, "Euclidean PI",
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"


SELECT * FROM film_category;

SELECT * FROM film;
INSERT INTO film_category VALUES(1001, 17);

UPDATE film_category
SET category_id = 17
WHERE film_id IN (SELECT film_id FROM film WHERE title IN ('Euclidean PI', 'EGG IGBY', 'KARATE MOON', 'RANDOM GO', 'YOUNG LANGUAGE'));
 


-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films
-- accordingly.
-- (5 rows affected)

SELECT * FROM film WHERE rating = 'G';
UPDATE film
SET rating = 'G'
WHERE film_id IN (SELECT film_id FROM film_category WHERE film_category.category_id = 17);

-- 7. Add a copy of "Euclidean PI" to all the stores.

SELECT * FROM inventory WHERE film_id = 1001;

INSERT INTO inventory (film_id, store_id)
VALUES (1001, 1), (1001, 2);


-- 8. The Feds have stepped in and have impounded all copies of the pirated film,
-- "Euclidean PI". The film has been seized from all stores, and needs to be
-- deleted from the film table. Delete "Euclidean PI" from the film table.
-- (Did it succeed? Why?)
-- <YOUR ANSWER HERE>

DELETE FROM film
WHERE film_id = 1001 and title = 'Euclidean PI';

-- failed due to foreign key error, we gotta delete this precious gem everywhere not just from film

-- 9. Delete Mathmagical from the category table.
-- (Did it succeed? Why?)
-- <YOUR ANSWER HERE>

DELETE FROM category
WHERE name = 'Mathmagical';

-- failed due to foreign key error, once again this needs to be deleted in more places than one

-- 10. Delete all links to Mathmagical in the film_category tale.
-- (Did it succeed? Why?)
-- <YOUR ANSWER HERE>

DELETE FROM film_category
WHERE category_id 

-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI".
-- (Did either deletes succeed? Why?)
-- <YOUR ANSWER HERE> 

DELETE FROM category
WHERE category.name = 'Mathmagical';

DELETE FROM film
WHERE title = 'Euclidean PI';

-- FAILED we used it in the film_actor table as well.
 
