package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FrontTimesTest {

	private FrontTimes frontTimes;
	
	@Before
	public void setup() {
		frontTimes = new FrontTimes();
	}

	//test large number
	@Test
	public void large_number_as_int() {
		
		String input = frontTimes.generateString("comment", 12);
		
		Assert.assertEquals("comcomcomcomcomcomcomcomcomcomcomcom", input);
	}
	
	//test small string
	@Test
	public void test_string_less_than_3() {
		
		String input = frontTimes.generateString("gh", 12);
		
		Assert.assertEquals("ghghghghghghghghghghghgh", input);
	}
	
	// test long string
	@Test
	public void test_large_string() {
		
		String input = frontTimes.generateString("thisisatestcasetotestthecase", 3);
		
		Assert.assertEquals("thithithi", input);
	}
	
	//test case sensitivity 
	@Test
	public void test_case_sensitivity() {
		
		String input = frontTimes.generateString("AnDn", 3);
		
		Assert.assertEquals("AnDAnDAnD", input);
	
		
	}
	
} 
