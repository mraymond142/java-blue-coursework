package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StringBitsTest {
	
	
	private StringBits stringBits;
	
	
	 @Before
	public void setup() {
		 stringBits = new StringBits();
	}
	 
		@Test
		public void test_string_returns_every_other_char() {
			
			String input = stringBits.getBits("Heeololeo");
			
			Assert.assertEquals("Hello", input);
		}
		
		
		@Test
		public void test_string_works_with_numbers() {
			 
			String input = stringBits.getBits("1234567");
			
			Assert.assertEquals("1357", input); 
		}
		
		
		@Test
		public void test_string_returns_every_other_char_works_with_case() {
			
			String input = stringBits.getBits("HeEoLoLeO");
			
			Assert.assertEquals("HELLO", input);
		}
		
		
		@Test
		public void tst_string_returns_one_char_given_one() {
			
			String input = stringBits.getBits("H");
			
			Assert.assertEquals("H", input);
		}
}
