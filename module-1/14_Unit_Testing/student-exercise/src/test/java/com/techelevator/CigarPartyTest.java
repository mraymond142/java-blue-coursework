package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CigarPartyTest {

	private CigarParty cigarParty;
	 
	@Before
	public void setup() {
		cigarParty = new CigarParty();
	}
	//test if under 40 and false returns false
	@Test
	public void under_forty_and_false() {
		
		boolean numCigar = cigarParty.haveParty(39, false);
		
		Assert.assertFalse(numCigar);
	}
	//test if over 60 and false returns false
	@Test
	public void over_sixty_and_false() {
		
		boolean numCigar = cigarParty.haveParty(61, false);
		
		Assert.assertFalse(numCigar);
	}
	//test 3000 and true returns true
	@Test
	public void upper_limit_and_true() {
		
		boolean numCigar = cigarParty.haveParty(300, true); 
		
		Assert.assertTrue(numCigar);
	}
	//test if under 40 and true returns false
	@Test
	public void below_limit_and_true() {
		
		boolean numCigar = cigarParty.haveParty(39, true);
		
		Assert.assertFalse(numCigar);
	}
	//test negative number
	@Test
	public void negative_and_true() {
		
		boolean numCigar = cigarParty.haveParty(-300, true);
		
		Assert.assertFalse(numCigar);
	}
	
	@Test
	public void negative_and_false() {
		
		boolean numCigar = cigarParty.haveParty(-300, false);
		
		Assert.assertFalse(numCigar);
	}
	
}
