package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class NonStartTest {
	
	private NonStart nonStart;
	
	@Before
	public void setup() {
		nonStart = new NonStart();
	}
	
	
	//test it returns concatenation without first letter
	@Test
	public void return_concatenation_without_first_of_each() {
		
		String result = nonStart.getPartialString("hello", "world");
		
		Assert.assertEquals("elloorld", result);
	}
	
	
	//test case senitivity
	@Test
	public void return_concatenation_with_varying_cases() {
		
		String result = nonStart.getPartialString("HeLlO", "wOrLd");
		
		Assert.assertEquals("eLlOOrLd", result);
	
	}
	
	
	//test with numbers
	@Test
	public void return_concatenation_with_numbers() {
		
		String result = nonStart.getPartialString("1234", "5678");
		
		Assert.assertEquals("234678", result);
	}
	
	
	//test short
	
	@Test
	public void return_concatenation_with_shortened_strings() {
		
		String result = nonStart.getPartialString("hi", "yo");
		
		Assert.assertEquals("io", result);
	}
	 
	//test long
	@Test
	public void return_concatenation_with_long_strings() {
		
		String result = nonStart.getPartialString("thisisatestthisisatest", "yesitisyesities");
		
		Assert.assertEquals("hisisatestthisisatestesitisyesities", result);
	}
	
	
	
	
	
	
	}
	
	
	
	
	
	

