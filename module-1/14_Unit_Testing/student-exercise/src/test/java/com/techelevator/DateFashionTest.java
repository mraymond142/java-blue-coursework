package com.techelevator;

	import org.junit.Assert;
	import org.junit.Before;
	import org.junit.Test;

public class DateFashionTest {
	 
	
		private DateFashion dateFashion;
		
		@Before
		public void setup() {
			dateFashion = new DateFashion();
		}
		
		
		//test if 2 for you is a no
		@Test
		public void two_or_below_you_return_zero() {
			
		int getTable = dateFashion.getATable(2, 10);
			
			Assert.assertEquals(0, getTable);
		}
		
		//test if 2 for date is a no
		@Test
		public void two_or_below_date_return_zero() {
			
			int getTable = dateFashion.getATable(10, 2);
			
			Assert.assertEquals(0, getTable);
		}
		
		//test 8 or more is yes
	@Test
		public void value_over_eight_return_2() {
		
		int getTable = dateFashion.getATable(10, 3);
		
		Assert.assertEquals(2, getTable);
	}
	
		//test date_over_eight
		@Test
		public void date_over_eight_return_2() {
			
		int getTable = dateFashion.getATable(3, 10);
	
			Assert.assertEquals(2, getTable);
	}
		
		//test negative number
		@Test
		public void negative_limit_test() {
			
			int getTable = dateFashion.getATable(-300, 8);
			
			Assert.assertEquals(0, getTable);
		}
		
	
		@Test
		public void large_number_test() {
			
		int getTable = dateFashion.getATable(300, 8);
		
		Assert.assertEquals(2, getTable);
	}
		
		@Test
		public void test_maybe_condition() {
			
		int getTable = dateFashion.getATable(4, 4);
		
		Assert.assertEquals(1, getTable);
	}
	
	
	
}
