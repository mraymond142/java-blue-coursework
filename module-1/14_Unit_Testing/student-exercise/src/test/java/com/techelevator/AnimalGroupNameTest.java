package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AnimalGroupNameTest {

	
	
		private AnimalGroupName animalGroupName;
		
		
		@Before
		public void setup() {
			animalGroupName = new AnimalGroupName();
		}
		
		
		//test different cases(upper, lower, hybrid)
		@Test
		public void any_case_returns_correct_packtype() {
			
			String getHerd = animalGroupName.getHerd("eLePhAnT");
			
			Assert.assertEquals("Herd", getHerd);
		}
		//test to make sure the herd type is correct
		
		@Test
		public void return_type_is_correct() { 
			
			String getHerd = animalGroupName.getHerd("Giraffe"); 
			
			Assert.assertEquals("Tower", getHerd);
		}
		//test a number
		
		@Test
		public void return_unknown_given_number() {
			
			String getHerd = animalGroupName.getHerd("23");
			
			Assert.assertEquals("unknown", getHerd);
		}
		//test a string not in the list
		@Test
		public void return_uknown_given_incorrect_string() {
			
			String getHerd = animalGroupName.getHerd("toaster");
			
			Assert.assertEquals("unknown", getHerd);
		}
}
