package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class Less20Test {
	
	private Less20 less20;
	
	@Before
	public void setup() {
		
		less20 = new Less20();
	}
	
	//test two less than a multiple of 20
	@Test
	public void test_two_less_than_multiple_of_20() {
		
		boolean input = less20.isLessThanMultipleOf20(18);
		
		Assert.assertTrue(input);
	}
	
	
	
	//test two less than a multiple of 20
	@Test
	public void test_one_less_than_multiple_of_20() {
		
		boolean input = less20.isLessThanMultipleOf20(19);
		
		Assert.assertTrue(input);
	}
	
	
	//test an exact multiple of 20
	@Test
	public void test_multiple_of_20() {
		
		boolean input = less20.isLessThanMultipleOf20(20);
		
		Assert.assertFalse(input);
	}
	
	
	
	// test a smaller number
	
	@Test
	public void test_less_than_multiple_of_20() {
		
		boolean input = less20.isLessThanMultipleOf20(16);
		
		Assert.assertFalse(input);
	}
	 
	
	
	//test a very large number
	@Test
	public void test_large_multiple_of_20() {
		
		boolean input = less20.isLessThanMultipleOf20(718);
		
		Assert.assertTrue(input);
	}

}
