package com.techelevator.calculator;



public class Calculator {

	private int result;

	
	public int add(int addend) {
		return result += addend;
	}
	
	public int multiply(int multiplier) {
		result *= multiplier; 
		return result ;
	}
	
	public int power(int exponent) {
		result = (int) Math.pow(result,exponent);
		
		return Math.abs(result); 
	}
	
	public void reset() {
		result = 0;
		
	}
	
	public int subtract(int subtrahend) {
		result -= subtrahend;
		return result;
	}
	
	
	public int getResult() {
		return result;
	}

}
