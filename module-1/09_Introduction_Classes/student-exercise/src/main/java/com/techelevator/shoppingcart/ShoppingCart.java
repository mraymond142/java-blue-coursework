package com.techelevator.shoppingcart;

public class ShoppingCart {
	
	private String shoppingCart;
	private int  totalNumberOfItems;
	private double totalAmountOwed;



	public String getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(String shoppingCart) {
		this.shoppingCart = shoppingCart;
	}
	
	
	
	public double getAveragePricePerItem() { 
		if (totalAmountOwed == 0 || totalNumberOfItems == 0) {
			return 0.0;
		} else {
	
			double average = (totalAmountOwed / totalNumberOfItems);
			return average;
		}
		
	}

	public int getTotalNumberOfItems() {
		return totalNumberOfItems;
	}
	
	public void empty() {
		totalNumberOfItems = 0;
		totalAmountOwed = 0.0;
	}

	

	public double getTotalAmountOwed() { 
		return totalAmountOwed;
	}

	

	public void addItems(int numberOfItems, double pricePerItem) {
		
		this.totalNumberOfItems += numberOfItems; 
		this.totalAmountOwed += pricePerItem * totalNumberOfItems;
		
	}

}
