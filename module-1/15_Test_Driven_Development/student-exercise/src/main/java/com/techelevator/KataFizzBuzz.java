package com.techelevator;

public class KataFizzBuzz {

	public String fizzBuzz(int input) {

		if (input < 0 ^ input > 100) {

			return "";
		}

		else if (input % 5 == 0 && 
					input % 3 == 0 || 
						("" + input).contains("3") && 
							("" + input).contains("5")) {

			return "FizzBuzz"; 

		} else if ((input % 5 == 0) || 
				("" + input).contains("5")) {

			return "Buzz";

		} else if ((input % 3 == 0) ||
				("" + input).contains("3")) {

			return "Fizz";

		} else if (input >= 0 && 
				input <= 100) {

			return Integer.toString(input);

		}
 
		return null;
	}

}
