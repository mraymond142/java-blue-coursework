package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KataFizzBuzzTest {
	
	private KataFizzBuzz kataFizzBuzz;
	
	@Before
	public void setup()
	{
		kataFizzBuzz = new KataFizzBuzz();
	}
	
	@Test 
	public void result_should_be_Fizz_when_number_is_3() {
		
		String fizzOrBuzz = kataFizzBuzz.fizzBuzz(3);
		
		Assert.assertEquals("Fizz", fizzOrBuzz);
		
		
	}
	
	@Test
	public void result_should_be_Buzz_when_number_is_5() {
		
		String fizzOrBuzz = kataFizzBuzz.fizzBuzz(5);
		
		Assert.assertEquals("Buzz", fizzOrBuzz);
	}
	
	@Test
	public void result_should_be_FizzBuzz_when_number_is_divisible_3_and_5() {
		
		String fizzOrBuzz = kataFizzBuzz.fizzBuzz(15);
		
		Assert.assertEquals("FizzBuzz", fizzOrBuzz); 
	
	}
	
	@Test
	public void convert_all_other_numbers_between_one_and_100_to_string() {
		
		String fizzOrBuzz = kataFizzBuzz.fizzBuzz(92);
		
		Assert.assertEquals("92", fizzOrBuzz);
	
	}
	
	@Test
	public void convert_all_other_numbers_not_between_one_and_100_to_empty_string() {
		
		String fizzOrBuzz = kataFizzBuzz.fizzBuzz(113);
		
		Assert.assertEquals("", fizzOrBuzz); 
		 
	}
	
	@Test
	public void return_Fizz_if_number_contains_a_3_or_divisible_by_3() {
		
		String fizzOrBuzz = kataFizzBuzz.fizzBuzz(73);
		
		Assert.assertEquals("Fizz", fizzOrBuzz); 
		
	}
	
	@Test
	public void return_Buzz_if_number_contains_a_5_or_divisible_by_5() {
		
		String fizzOrBuzz = kataFizzBuzz.fizzBuzz(51);
		
		Assert.assertEquals("Buzz", fizzOrBuzz); 
	
	}
	
	@Test
	public void return_FizzBuzz_if_number_contains_5_or_3_or_divisible_by_5_and_3() {
		
		String fizzOrBuzz = kataFizzBuzz.fizzBuzz(35);
		
		Assert.assertEquals("FizzBuzz", fizzOrBuzz); 
	
	}
	
}
