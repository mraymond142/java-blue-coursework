package com.techelevator;
import java.util.Scanner;
public class DecimalToBinary {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Please enter in a series of decimal values separated by spaces");
		String userInput = in.nextLine();
		
		String[] splitInputs = userInput.split(" ");
		
		int[] inputArray = new int[splitInputs.length];
		
		for(int i = 0; i < splitInputs.length; i++) {
			
		    inputArray[i] = Integer.parseInt(splitInputs[i]);
		}
		
		
		
		for (int i = 0; i <= inputArray.length - 1; i++) {
			
			System.out.println(inputArray[i] + " in binary is " + Integer.toBinaryString(inputArray[i]));
			
			
		}
			in.close();
	
	}
	
		

	} 


