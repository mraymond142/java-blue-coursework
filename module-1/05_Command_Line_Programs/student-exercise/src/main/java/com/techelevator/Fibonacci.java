package com.techelevator;
import java.util.Scanner;
public class Fibonacci {

	public static void main(String[] args) {
	Scanner in = new Scanner(System.in);
	int[] fibonacciArray = new int[]{ 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811};
	
	System.out.println("Please Enter the Fibonacci number:");
	String userInput = in.nextLine();
	int valueInput = Integer.parseInt(userInput);
	
	for (int i = 0; fibonacciArray[i] <= valueInput; i++) {
	
		System.out.print(fibonacciArray[i]+ " ");
	}
	in.close();  
	}
	

}
