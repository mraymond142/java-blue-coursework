package com.techelevator;
import java.text.DecimalFormat;
import java.util.Scanner;
public class LinearConvert {

	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in); 
		
		
		DecimalFormat df2 = new DecimalFormat("#.##");
		double meterCalc = 0.3048;
		double footCalc = 3.2808399; 
		
		
		
		System.out.println("Enter a length >  ");
		
		String userInput = in.nextLine();
		int valueInput = Integer.parseInt(userInput);
		
		System.out.println("Is that in (m)eters or (f)eet? >");
		
		String measurementType = in.nextLine();
		
		double footToMeter = meterCalc * valueInput;
		double meterToFoot = footCalc * valueInput;
		
		if (measurementType.equals("m")) 
			
		System.out.printf(valueInput +"m " + "is " + df2.format(meterToFoot) + "f");
		
		 
		if (measurementType.equals("f"))
		
		System.out.printf(valueInput +"f " + "is " + df2.format(footToMeter) + "m");
		
	
		
		in.close(); 
		 
	}
}
