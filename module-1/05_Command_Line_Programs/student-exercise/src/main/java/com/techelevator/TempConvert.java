package com.techelevator;
import java.util.Scanner;
import java.text.DecimalFormat;
public class TempConvert {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		
		DecimalFormat df2 = new DecimalFormat("#.##");
		
		System.out.println("Enter a temperature >  ");
		
		String userInput = in.nextLine();
		int valueInput = Integer.parseInt(userInput);
		
		System.out.println("Is that temperature in (F)ahrenheit or (C)elsius? >");
		
		String measurementType = in.nextLine();
		
		double fahrenheitConvert = (valueInput - 32) / 1.8;
		double celsiusConvert = (valueInput * 1.8) + 32;
		
		
		if (measurementType.equals("F")) 
			
		System.out.println( valueInput +"F " + "is " +  df2.format(fahrenheitConvert) + "C");
		
		 
		if (measurementType.equals("C"))
		
		
		System.out.println(valueInput + "C" + " is " +  df2.format(celsiusConvert) + "F");
		
	
		
		in.close(); 
	} 
		
		
	}


