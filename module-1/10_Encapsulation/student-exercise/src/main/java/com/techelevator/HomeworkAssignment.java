package com.techelevator;

public class HomeworkAssignment {
	
	private final static String LOWEST_LETTER_GRADE = "F";
	
	private int totalMarks;
	private int possibleMarks;
	private String submitterName;
	 
	public HomeworkAssignment(int possibleMarks) {
		
		this.possibleMarks = 100; 
	}

		public int getTotalMarks() {
			return totalMarks;
	}
		public void setTotalMarks(int totalMarks) {
			this.totalMarks = totalMarks;
	}
		public String getSubmitterName() {
			return submitterName;
	}
		public void setSubmitterName(String submitterName) {
			this.submitterName = submitterName;
	}
		public int getPossibleMarks() {
			return possibleMarks;
	}
		
		
		public String getLetterGrade() {
			
			Double totalMarksDouble = Double.valueOf(totalMarks);
		
			Double possibleMarksDouble = Double.valueOf(possibleMarks);
		
			double preStringLetterGrade = totalMarksDouble / possibleMarksDouble;
		
			if (preStringLetterGrade >= .9) {
				return "A";
			
			} if (preStringLetterGrade >= .8 && preStringLetterGrade <= .89) {
				return "B";
				
				} if (preStringLetterGrade >= .7 && preStringLetterGrade <= .79) {
					return "C";
					
				} if (preStringLetterGrade >= .6 && preStringLetterGrade <= .69) {
					return "D";
					
				} else return LOWEST_LETTER_GRADE;
		
	}

}
