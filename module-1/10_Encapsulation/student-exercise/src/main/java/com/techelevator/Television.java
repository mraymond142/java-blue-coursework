package com.techelevator;

public class Television {
	
	private final static int STARTING_CHANNEL = 3;
	private final static int STARTING_VOLUME = 2;
	private final static int MAX_VOLUME = 18;
	
	private boolean isOn = false;
	private int currentChannel = STARTING_CHANNEL; 
	private int currentVolume = 2;
	
    public void turnOff() {
    	isOn = false;
    }
    
   public void turnOn() {
	   isOn = true;
	   currentChannel = STARTING_CHANNEL;
	   currentVolume = STARTING_VOLUME;
	   
   }
   
   public void changeChannel(int newChannel) {
	   if (isOn && currentChannel >= 3 && currentChannel <= 18) {
		   currentChannel = newChannel;
	   }
	   
   }
   
   public void channelUp() {
	   if (isOn) {
		   currentChannel += 1;
	   if (isOn && currentChannel > MAX_VOLUME) {
		   currentChannel = 3;
	   }
	   }
   }
   
    public void channelDown() {
    	if (isOn) {
    		currentChannel--; 
    	if (isOn && currentChannel < STARTING_CHANNEL) 
    		currentChannel = 18;
    	}
    	
    }
    
   public void raiseVolume() {
	   if (isOn) {
		   if (currentVolume + 1 > 10) {
			   currentVolume = 10;
		   }else {
			   currentVolume++;
		   }
	   } 
	   
   } 
   
    public void lowerVolume() {
    	if (isOn) {
    		currentVolume--;
    		if (currentVolume < 0) {
    			currentVolume = 0;
    		} 
    	}
    	
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public boolean isOn() {
		return this.isOn;
	}
	public int getCurrentChannel() {
		return this.currentChannel;
	}
	public int getCurrentVolume() {
		return this.currentVolume;
	}
	

}
