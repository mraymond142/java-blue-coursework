package com.techelevator;

public class Airplane {
	
	private String planeNumber;	
	private int bookedFirstClassSeats;	
	private int availableFirstClassSeats;
	private int totalFirstClassSeats;	
	private int bookedCoachSeats;	
	private int availableCoachSeats; 
	private int totalCoachSeats;
	
	
	public Airplane(String planeNumber, int totalFirstClassSeats, int totalCoachSeats) { 
		
		this.availableCoachSeats = totalCoachSeats - bookedCoachSeats;
		
		this.availableFirstClassSeats = totalFirstClassSeats - bookedFirstClassSeats;
		
		this.totalCoachSeats = totalCoachSeats;
		
		this.totalFirstClassSeats = totalFirstClassSeats;
		
		this.planeNumber = planeNumber;
}
	
	
	
	public boolean reserveSeats(boolean forFirstClass, int totalNumberOfSeats) {
		
		if (forFirstClass && totalNumberOfSeats <= availableFirstClassSeats) {
			
			availableFirstClassSeats -= totalNumberOfSeats;
			
			bookedFirstClassSeats += totalNumberOfSeats;
			
			return true;
			
		} else if (forFirstClass && totalNumberOfSeats > availableFirstClassSeats)  {
			return false;
		}
			
		 if (!forFirstClass && totalNumberOfSeats <= availableCoachSeats) {
			 
			availableCoachSeats -= totalNumberOfSeats;
			
			bookedCoachSeats += totalNumberOfSeats;
			
			return true;
			
		 } else if (!forFirstClass && totalNumberOfSeats > availableCoachSeats) {
			 
		 }
		 return false;
	
	
	
	}
	
	
	public String getPlaneNumber() {
		return planeNumber;
	}
	public int getBookedFirstClassSeats() {
		return bookedFirstClassSeats;
	}
	public int getAvailableFirstClassSeats() {
		return availableFirstClassSeats;
	}
	public int getTotalFirstClassSeats() {
		return totalFirstClassSeats;
	}
	public int getBookedCoachSeats() {
		return bookedCoachSeats;
	}
	public int getAvailableCoachSeats() {
		return availableCoachSeats;
	}
	public int getTotalCoachSeats() {
		return totalCoachSeats;
	}
	
	

}
