package com.techelevator;

public class FruitTree {
	
	
	
	private String typeOfFruit;
	private int piecesOfFruitLeft;

	
	public FruitTree(String typeOfFruit, int startingPiecesOfFruit) {
		this.typeOfFruit = typeOfFruit;
		this.piecesOfFruitLeft = startingPiecesOfFruit;
	
	}
	public boolean pickFruit(int numberOfPiecesToRemove) {
		
		boolean fruitLeft = false; 
	
		if (numberOfPiecesToRemove <= piecesOfFruitLeft) {
			
			 piecesOfFruitLeft -= numberOfPiecesToRemove;
		
			 return fruitLeft = true;
			 
		}  else return fruitLeft;
		} 
		
	
	public String getTypeOfFruit() {
		return this.typeOfFruit;
	}
	public int getPiecesOfFruitLeft() {
		return this.piecesOfFruitLeft;
	}

}
